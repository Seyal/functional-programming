package org.tcl;

import java.util.function.Function;

public class PassBehaviour {

    public static void main(String[] args) {
        int begin = 10;
        int end = 15;

        //10+11+12+13+14+15
        int result = sumOf(begin, end);
        assert result == 75;

        //10^2+11^2+12^3+13^2+14^2+15^2
        result = sqOf(begin, end);
        assert result == 955;

        //Passing behaviour
        result = genericOp(begin, end, (a)->a );
        assert result == 75;

        //10^2+11^2+12^3+13^2+14^2+15^2
        result = genericOp(begin, end, (a)->a * a);
        assert result == 955;

    }

    private static int sumOf(int a, int b) {
        int result = 0;
        for (int i = a; i <= b; i++) {
            result += i;
        }
        return result;
    }

    private static int sqOf(int a, int b) {
        int result = 0;
        for (int i = a; i <= b; i++) {
            result += i * i;
        }
        return result;
    }


    private static int genericOp(int a, int b, Function<Integer, Integer> operator){
        int result = 0;
        for (int i = a; i <= b; i++) {
            result += operator.apply(i);
        }
        return result;
    }
}
